include("data.jl")

data = getprocesseddata()

# Normally we would want the final output to have 10 activations
# and we would use cross-entropy against them

# To simplify things we will use mean squared error -> 1 activation

x_train = data.x_train
y_train = data.y_train

num_input = size(x_train)[begin]
classes = length(unique(y_train))

num_hidden = 50

# Initialization is extremely important

W1 = randn(num_hidden, num_input) / sqrt(num_input)
b1 = zeros(num_hidden)
W2 = randn(1, num_hidden) / sqrt(num_hidden)
b2 = zeros(1)

lin(
    A::AbstractMatrix{<:AbstractFloat},
    W::AbstractMatrix{<:AbstractFloat},
    b::AbstractVector{<:AbstractFloat},
) = W * A .+ b

@time t = lin(x_train, W1, b1)
