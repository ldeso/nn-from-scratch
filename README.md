# NN From Scratch

A (soon to be) state-of-the-art neural network programmed from scratch.

The network is implemented in both Julia and Python. These implementations can
be found in the directories of the same name.

## License

This project is licensed under the [Apache License 2.0](https://gitlab.com/de-souza/nn-from-scratch/blob/master/LICENSE).
