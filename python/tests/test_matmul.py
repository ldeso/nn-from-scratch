import unittest
import torch
from nnfromscratch import matmul


class MatMulTestCase(unittest.TestCase):
    def setUp(self):
        self.mat_a = torch.rand(5, 3)
        self.mat_b = torch.rand(3, 2)

    def test_matmul_correct(self):
        mat_c = matmul.matmul(self.mat_a, self.mat_b)
        self.assertTrue(mat_c.equal(self.mat_a @ self.mat_b))

    def test_matmul_mismatch(self):
        self.assertRaises(ValueError, matmul.matmul, self.mat_b, self.mat_a)
